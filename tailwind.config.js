module.exports = {
    content: [
      "./src/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
      extend: {
        fontFamily:{
          pixel:["VT323"], 
          cali:["Shadows Into Light"],
          cursive:["Ms Modi", "cursive"],//, "Square Peg"],
          console:["Iconsolata",'monospace']
        },
        colors:{
          'kindle-bg':'#f3f3f3',
          'kindle-text':'#323232'
        },
        screens: {
          'sm': '640px', // => @media (min-width: 640px) { ... }
          'md': '768px', // => @media (min-width: 768px) { ... }
          'lg': '1024px', // => @media (min-width: 1024px) { ... }
          'xl': '1024px', // => @media (min-width: 1280px) { ... }
          '2xl': '1024px', // => @media (min-width: 1280px) { ... }
  
      }
      },
    },
    plugins: [],
  }
  