const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
console.log("directory name: ", path.resolve(__dirname, "dist/"));
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: "./src/index",
  mode: "development",
  // content: ["./src/**/*.{html,js}"],
  // optimization: {
  //     minimize:true, //dev mode opt
  // },
  output: {
    path: path.resolve(__dirname, "dist/"),
    publicPath: "/dist/",
    filename: "main.bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_componenets)/,
        loader: "babel-loader",
        options: { presets: ["@babel/env"] },
      },
      {
        test: /.s?css$/,
        use: ["style-loader", "css-loader","postcss-loader"],
      },
      {
        test: /\.(png|jpg|jpe?g|gif)$/i,
        loader: 'file-loader',
      }, {
        test: /\.(ts|tsx)?$/,
        use:'ts-loader',
        exclude:/node_modules/,
      }
    ],
  },
  resolve: {
    extensions:['.tsx','.ts','.js'],
  },
  devServer: {
    static: {
      directory: path.join(__dirname, "public"),
    },
    //reacter route
    historyApiFallback:true,
    compress: true,
    port: 8000,
  },
  plugins: [
    new HtmlWebpackPlugin({ template: "./src/index.html" })
  ],
};
