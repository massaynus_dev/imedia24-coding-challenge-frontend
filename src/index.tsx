import * as React from "react";
import * as ReactDOM from "react-dom/client";
import { store } from './store';
import { Provider } from 'react-redux';
import App from "./App";



const root = ReactDOM.createRoot(document.getElementById("root")!);
root.render(
    <Provider store={store}>
        <React.StrictMode>
                <App />
        </React.StrictMode>
    </Provider>

);