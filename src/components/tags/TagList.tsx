import * as React from "react";

const TagList = (props: any) => {
    return (
      <div className=" text-slate-700 text-start mb-2">
        <h3 className="p-1 blockuppercase font-bold uppercase">abilities</h3>
        <div>
          {props.children}
        </div>
      </div>
    )
  };

  export default TagList;