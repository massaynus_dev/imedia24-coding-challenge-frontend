import * as React from "react";
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Card from "./Card";

test('loads and displays a card', async () => {
    const props = {
        title: "Testing",
        url: "/Testing",
        onClick: () => console.log("clicked")
    };
    render(<Card {...props} />);
    await screen.findByTestId('card');
});