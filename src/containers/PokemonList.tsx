import * as React from "react";
import { Card, Grid } from "../components/cards";
import { connect } from "react-redux";
import { getPokemonFetch, getPokemonsFetch } from "../features/PokemonSlice";
import { bindActionCreators, Dispatch } from "@reduxjs/toolkit";
import { RootState } from "../store";
import PokemonModal from "./PokemonModal";

interface Props {
    dispatch:any;
    limit:number;
    pokemons:any;
    selectedPokemon:any;
    isLoading:boolean;

};

interface State {
    hidden: boolean;
    scrollTop: number;
    oldDelta:number;
};

class PokemonList extends React.Component<Props, State> {

    private scrollRef: React.RefObject<HTMLDivElement>;
    constructor(props: Props) {
        super(props);
        this.scrollRef = React.createRef();
        this.state = {
            hidden: true,
            scrollTop: 0,
            oldDelta:0,
        }
    }
    handleScroll = (event: WheelEvent) => {
        // console.log(scrollRef.current.getScrollableNode().children[0].scrollTop);
        if (window.scrollY>this.scrollRef.current.clientHeight-window.innerHeight && !this.props.isLoading) {
            this.props.dispatch(getPokemonsFetch({ limit: this.props.limit }));
            console.log("here");
        }
        this.setState({ oldDelta: event.deltaY });
    };
    componentDidMount(): void {
        this.props.dispatch(getPokemonsFetch({ limit: this.props.limit }));
        this.scrollRef.current.addEventListener('wheel', this.handleScroll, { passive: false });
    }
    handleClick = (e: React.MouseEvent<HTMLButtonElement>, url: string) => {
        this.props.dispatch(getPokemonFetch({ url: url }));
        this.setState({ hidden: !this.state.hidden });
    }
    render(): React.ReactNode {
        return (
            <>
                <div ref={this.scrollRef} className="flex mb-4 container mx-auto flex-wrap ">
                    <h2 className="w-full text-center font-bold text-lg m-4 ml-0 text-slate-700">List of Pokemons:</h2>
                    <Grid rows={2}>
                        {this.props.pokemons.map((pokemon: any) => <Card title={pokemon.name} url={pokemon.url} onClick={this.handleClick} />)}
                    </Grid>
                    <PokemonModal pokemon={this.props.selectedPokemon} hidden={this.state.hidden} setHidden={(hidden: boolean) => this.setState({ hidden: hidden })} />
                </div>
            </>
        );
    }
}


const actions: any = Object.assign({}, getPokemonFetch,getPokemonsFetch);


function mapStateToProps(state: RootState) {
    const { pokemons,limit,selectedPokemon,isLoading } = state.pokemons
    return { pokemons: pokemons,limit,selectedPokemon,isLoading }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(mapStateToProps)(PokemonList)