import { Action } from "@redux-saga/types";
import { PayloadAction } from "@reduxjs/toolkit";
import {call,put,takeEvery} from "redux-saga/effects";
import { getPokemonsSuccess,getPokemonSuccess } from "../features/PokemonSlice";


function* workGetPokemonsFetch(action:any):Generator<any,any,any>{
    const pokemons = yield call(() => fetch('https://pokeapi.co/api/v2/pokemon?limit='+action.payload.limit));
    const formattedPokemons = yield pokemons.json();
    yield put(getPokemonsSuccess(formattedPokemons));
}

function* workGetPokemonFetch(action:any):Generator<any,any,any>{
    const pokemon = yield call(() => fetch(action.payload.url));
    const formattedPokemon = yield pokemon.json();
    console.log(formattedPokemon);
    yield put(getPokemonSuccess(formattedPokemon));
}
function*  pokeSaga(){
    yield takeEvery('pokemons/getPokemonsFetch', workGetPokemonsFetch);
    yield takeEvery('pokemons/getPokemonFetch', workGetPokemonFetch);
}

export default pokeSaga;